package info.guardianproject.keanu.core.ui.quickresponse


import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import info.guardianproject.keanu.core.ui.room.MessageAction
import info.guardianproject.keanu.core.ui.room.RoomVieModel
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentQuickResponseBinding
import org.matrix.android.sdk.api.session.room.model.message.*
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageBody
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import java.net.URLConnection
import java.util.*

class QuickResponseFragment : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentQuickResponseBinding
    private val roomViewModel: RoomVieModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val timeline = roomViewModel.getSelectedMessage()
        binding = FragmentQuickResponseBinding.inflate(layoutInflater)
        if (timeline != null) {
            populateResponseView(timeline)
        }
        binding.emojiRecycleView.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        val emojiAdapter = EmojiAdapter { position -> onEmojiClickListener(position) }
        emojiAdapter.recentEmojiList = roomViewModel.getEmojiList()
        binding.emojiRecycleView.adapter = emojiAdapter
        roomViewModel.imageUri.observe(this, {
            if (!Uri.EMPTY.equals(it)) {
                binding.messageImageView.visibility = View.VISIBLE
                GlideUtils.loadImageFromUri(context, it, binding.messageImageView)
            } else {
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.GONE
            }


        })
        binding.addReactionTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.ADD_REACTION
            roomViewModel.selectedEmoji.value = null
            dismiss()

        }
        binding.replyTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.REPLY
            dismiss()
        }
        binding.copyTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.COPY
            dismiss()
        }
        binding.fwdTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.FORWARD
            dismiss()
        }
        binding.deleteTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.DELETE
            dismiss()

        }
        return binding.root

    }

    private fun onEmojiClickListener(position: Int) {
        roomViewModel.selectedEmoji.value = roomViewModel.getEmojiList()[position]
        roomViewModel.messageAction.value = MessageAction.QUICK_REACTION
        dismiss()

    }

    private fun populateResponseView(timeline: TimelineEvent) {
        val timestamp = timeline.root.originServerTs
        val senderName =
            if (timeline.senderInfo.displayName.isNullOrBlank()) timeline.senderInfo.userId
            else timeline.senderInfo.displayName
        binding.messageSenderTextView.text = senderName
        if (timestamp != null) {
            binding.messageTimeTextView.text = PrettyTime.format(Date(timestamp), requireContext())
        } else {
            binding.messageContentTextView.text = "-:-"
        }
        val message = if (timeline.getLastMessageBody()?.startsWith(">")!!) {
            timeline.getLastMessageBody().toString().split("\n\n")[1]
        } else {
            timeline.getLastMessageBody()
        }

        binding.messageContentTextView.text = message

        when (val messageContent = timeline.getLastMessageContent()) {
            is MessageAudioContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.GONE
                roomViewModel.imageUri.value = Uri.EMPTY
                binding.messageTypeTextView.text = messageContent.audioInfo?.size?.let {
                    "${roomViewModel.getFileSizeInKb(it)} kB"
                }

            }

            is MessageImageContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE
                val imageInfo = messageContent.info
                val messageInfo = "${imageInfo?.width} x ${imageInfo?.height} - ${
                    imageInfo?.size?.let {
                        roomViewModel.getFileSizeInKb(
                            it
                        )
                    }
                } kB"
                binding.messageTypeTextView.text = messageInfo
                roomViewModel.getSelectedMessageImageUri(messageContent = messageContent)

            }

            is MessageStickerContent -> {
                binding.messageTypeTextView.visibility = View.GONE
                binding.messageImageView.visibility = View.VISIBLE
                roomViewModel.getSelectedMessageImageUri(messageContent = messageContent)

            }

            is MessageVideoContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE
                val videoInfo = messageContent.videoInfo
                val messageInfo = "${videoInfo?.width} x ${videoInfo?.height} -  ${
                    videoInfo?.size?.let {
                        roomViewModel.getFileSizeInKb(
                            it
                        )
                    }
                } kB"
                binding.messageTypeTextView.text = messageInfo
                roomViewModel.getSelectedMessageImageUri(messageContent = messageContent)
            }
            is MessageFileContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE
                roomViewModel.imageUri.value = Uri.EMPTY
                val mimeType = getMimeType(messageContent.mimeType, messageContent.body)
                setAttachmentImage(mimeType)
                val fieInfo = messageContent.info
                val messageInfo = "${
                    fieInfo?.size?.let {
                        roomViewModel.getFileSizeInKb(
                            it
                        )
                    }
                } kB"
                binding.messageTypeTextView.text = messageInfo
            }

            is MessageWithAttachmentContent -> {
                binding.messageTypeTextView.visibility = View.GONE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE
                roomViewModel.imageUri.value = Uri.EMPTY
                val mimeType = getMimeType(messageContent.mimeType, messageContent.body)
                setAttachmentImage(mimeType)

            }

            else -> {
                binding.messageTypeTextView.visibility = View.GONE
                binding.messageContentTextView.visibility = View.GONE
                binding.messageImageView.visibility = View.VISIBLE
                roomViewModel.getSelectedImageContent(timeline.getLastMessageBody(), context)

            }
        }


    }

    private fun setAttachmentImage(mimeType: String) {
        when {
            mimeType.isEmpty() -> {
                binding.messageImageView.setImageResource(R.drawable.file_unknown)
            }

            mimeType.contains("html") -> {

                binding.messageImageView.setImageResource(R.drawable.file_unknown)

            }

            mimeType == "text/csv" -> {

                binding.messageImageView.setImageResource(R.drawable.proofmodebadge)
            }

            mimeType.contains("pdf") -> {
                binding.messageImageView.setImageResource(R.drawable.file_pdf)
            }

            mimeType.contains("doc") || mimeType.contains("word") -> {
                binding.messageImageView.setImageResource(R.drawable.file_doc)
            }

            mimeType.contains("zip") -> {
                binding.messageImageView.setImageResource(R.drawable.file_zip)
            }

            else -> {
                binding.messageImageView.setImageResource(R.drawable.file_unknown)
            }
        }

    }

    companion object {
        @JvmStatic
        fun newInstance(): QuickResponseFragment {
            return QuickResponseFragment()
        }
    }

}

fun getMimeType(mimeType: String?, filename: String?): String {
    if (filename?.isNotEmpty() == true && (mimeType.isNullOrEmpty() || mimeType.startsWith("application"))) {
        val guessed = URLConnection.guessContentTypeFromName(filename)

        if (guessed?.isNotEmpty() == true) {
            return if (guessed == "video/3gpp") "audio/3gpp" else guessed
        }
    }

    return if (mimeType == "video/3gpp") "audio/3gpp" else mimeType ?: ""
}

interface QuickResponseLongClickListener {
    fun onQuickResponseClick(timeline: TimelineEvent)
}

