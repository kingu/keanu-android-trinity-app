package info.guardianproject.keanu.core.ui.room

import android.content.Context
import android.net.Uri
import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import info.guardianproject.keanu.core.ImApp
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.model.message.*
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.internal.crypto.attachments.toElementToDecrypt
import java.util.*

enum class MessageAction {
    QUICK_REACTION, ADD_REACTION, COPY, REPLY, FORWARD, DELETE
}

class RoomVieModel : ViewModel() {
    private val mSession: Session?
        get() = ImApp.sImApp?.matrixSession

    val imageUri = MutableLiveData<Uri>()
    val selectedMessage = MutableLiveData<TimelineEvent>()
    val selectedEmoji = MutableLiveData<String>()
    val messageAction = MutableLiveData<MessageAction>()
    fun getSelectedMessage(): TimelineEvent? {
        return selectedMessage.value
    }

    fun getSelectedMessageImageUri(messageContent: MessageImageContent) {
        viewModelScope.launch {
            val url = messageContent.info?.thumbnailUrl ?: messageContent.getFileUrl()
            val fileName = messageContent.getFileName()
            if (url?.isNotEmpty() == true && url.startsWith("mxc")) {
                val fileMedia = mSession?.fileService()?.downloadFile(
                    fileName,
                    messageContent.mimeType,
                    url,
                    messageContent.info?.thumbnailFile?.toElementToDecrypt()
                )

                imageUri.value = Uri.fromFile(fileMedia)
            }


        }
    }

    fun getEmojiList(): List<String> {
        return listOf(

            "\uD83D\uDC4D",
            "\uD83D\uDC4E",
            "\uD83D\uDE04",
            "\uD83C\uDF89",
            "\uD83D\uDE15",
            "\uD83D\uDE80",
            "\uD83D\uDC40",
        )
    }

    fun getSelectedMessageImageUri(messageContent: MessageVideoContent) {
        val videoInfo = messageContent.videoInfo
        viewModelScope.launch {
            val url = videoInfo?.thumbnailUrl ?: messageContent.videoInfo?.thumbnailFile?.url
            val mimeType = videoInfo?.thumbnailInfo?.mimeType
            val fileName = "thumb" + messageContent.getFileName()


            if (url?.isNotEmpty() == true && url.startsWith("mxc")) {
                val fileMedia = mSession?.fileService()?.downloadFile(
                    fileName,
                    mimeType,
                    url,
                    messageContent.videoInfo?.thumbnailFile?.toElementToDecrypt()
                )

                imageUri.value = Uri.fromFile(fileMedia)
            }


        }
    }

    fun getSelectedMessageImageUri(messageContent: MessageStickerContent) {
        val messageInfo = messageContent.info
        viewModelScope.launch {
            val url = messageInfo?.thumbnailUrl ?: messageContent.info?.thumbnailFile?.url
            val mimeType =
                messageInfo?.thumbnailInfo?.mimeType ?: messageInfo?.thumbnailFile?.url
            val fileName = "thumb" + messageContent.getFileName()

            if (url?.isNotEmpty() == true && url.startsWith("mxc")) {
                val fileMedia = mSession?.fileService()?.downloadFile(
                    fileName,
                    mimeType,
                    url,
                    messageContent.info?.thumbnailFile?.toElementToDecrypt()
                )

                imageUri.value = Uri.fromFile(fileMedia)
            }


        }
    }

    fun getFileSizeInMb(number: Long): Double {
        return getFileSizeInKb(number) / 1000
    }

    fun getFileSizeInKb(number: Long): Double {
        return (number / 1000).toDouble()
    }

    fun getSelectedImageContent(message: String?, context: Context?) {
        var asset: String? = null

        if (message?.startsWith("/sticker:") == true) {
            // Just get up to any whitespace.
            asset = message.split(":").toTypedArray()[1].split(" ").toTypedArray().first()
                .lowercase(Locale.ROOT)
        } else if (message?.startsWith(":") == true) {
            val parts = message.split(":").toTypedArray()[1].split("-").toTypedArray()
            val folder = parts.firstOrNull()
            val name = StringBuffer()

            for (i in 1 until (parts.size)) {
                name.append(parts[i])
                if (i + 1 < (parts.size)) name.append('-')
            }

            asset = "stickers/$folder/$name.png"
        }

        if (asset != null && assetExists(asset, context)) {
            imageUri.value = "asset://localhost/$asset".toUri()

        } else {
            imageUri.value = Uri.EMPTY
        }
    }

    private fun assetExists(asset: String, context: Context?): Boolean {
        return try {
            val fd = context?.assets?.openFd(asset) ?: return false
            fd.length
            fd.close()

            true
        } catch (failure: Throwable) {
            false
        }
    }

}