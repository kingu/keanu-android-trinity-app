package info.guardianproject.keanu.core.util

import agency.tango.android.avatarview.AvatarPlaceholder
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import info.guardianproject.keanu.core.ImApp
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.room.sender.SenderInfo
import timber.log.Timber
import java.io.File
import java.io.FileInputStream

object GlideUtils {

    private var noDiskCacheOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

    fun loadAvatar(av: ImageView, senderInfo: SenderInfo) {
        val senderName = if (senderInfo.displayName.isNullOrBlank()) senderInfo.userId else senderInfo.displayName

        loadAvatar(av.context, senderInfo.avatarUrl, AvatarPlaceholder(senderName), av)
    }

    @JvmStatic
    fun loadAvatar(context: Context, avatarUrl: String?, default: Drawable, av: ImageView) {
        val glideRb = Glide.with(context).asDrawable()

        @SuppressLint("CheckResult")
        if (avatarUrl?.isNotEmpty() == true) {
            glideRb.load(resolve(avatarUrl.toUri(), true))
        }
        else {
            glideRb.load(default)
        }

        glideRb.into(av)
    }

    @SuppressLint("CheckResult")
    @JvmStatic
    fun loadImageFromUri(context: Context?, uri: Uri, imageView: ImageView?) {
        if (context == null) return
        if (imageView == null) return

        val progress = CircularProgressDrawable(context)
        progress.strokeWidth = 5f
        progress.centerRadius = 30f
        progress.start()

        val glideRb = Glide.with(context)
                .asDrawable()
                .placeholder(progress)
                .apply(noDiskCacheOptions)

        if (SecureMediaStore.isVfsUri(uri)) {
            try {
                val path = uri.path ?: return
                val fileImage = File(path)

                if (fileImage.exists()) {
                    glideRb.load(FileInputStream(fileImage))
                }
            } catch (e: Exception) {
                Timber.w(e, "unable to load image: $uri")
            }
        }
        else {
            glideRb.load(resolve(uri, false))
        }

        glideRb.into(imageView)
    }

    private fun resolve(uri: Uri, asThumbnail: Boolean): Uri {
        return when (uri.scheme) {
            "mxc" -> {
                val r  = ImApp.sImApp?.matrixSession?.contentUrlResolver()

                return (
                        if (asThumbnail) {
                            r?.resolveThumbnail(uri.toString(), 120, 120,
                                    ContentUrlResolver.ThumbnailMethod.SCALE)
                        }
                        else {
                            r?.resolveFullSize(uri.toString())
                        }
                        )?.toUri() ?: uri
            }

            "asset" -> ("file:///android_asset/" + uri.path?.substring(1)).toUri()

            else -> uri
        }
    }
}