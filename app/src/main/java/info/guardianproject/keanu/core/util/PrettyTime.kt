package info.guardianproject.keanu.core.util

import android.content.Context
import android.os.Build
import info.guardianproject.keanu.core.ui.gallery.GalleryActivity
import org.ocpsoft.prettytime.PrettyTime
import java.lang.ref.WeakReference
import java.util.*

class PrettyTime {

    companion object {

        private var mLocale: Locale? = null

        private var mPrettyTime: PrettyTime? = null

        fun format(date: Date?, context: Context? = null): String? {
            val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                context?.resources?.configuration?.locales?.get(0)
            }
            else {
                @Suppress("DEPRECATION")
                context?.resources?.configuration?.locale
            }

            if (mPrettyTime == null || mLocale != locale) {
                mLocale = locale
                mPrettyTime = PrettyTime(locale)
            }

            return mPrettyTime?.format(date)
        }
    }
}