package info.guardianproject.keanu.core.ui.room

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AwesomeActivityStoryDetailBinding
import info.guardianproject.keanuapp.ui.conversation.StoryViewContrib

/**
 * Created by N-Pex on 2019-03-29.
 */
open class StoryActivity : RoomActivity() {

    companion object {
        // Use this flag as a boolean EXTRA to enable contributor mode (as opposed to viewer mode)
        const val EXTRA_CONTRIBUTOR_MODE = "contributor_mode"
    }

    private lateinit var mStoryBinding: AwesomeActivityStoryDetailBinding

    override val root
        get() = mStoryBinding.root

    override val toolbar
        get() = mStoryBinding.toolbar

    override val inputLayout
        get() = mStoryBinding.inputLayout

    override val historyView
        get() = mStoryBinding.history

    override val composeMessage
        get() = mStoryBinding.composeMessage

    override val btnSend
        get() = mStoryBinding.btnSend

    override val btnMic
        get() = mStoryBinding.btnMic

    open val progressBar
        get() = mStoryBinding.progressHorizontal

    open val previewAudio
        get() = mStoryBinding.previewAudio

    open val storyAudioPlayerView
        get() = mStoryBinding.audioPlayerView

    open val fabShowAudioPlayer
        get() = mStoryBinding.fabShowAudioPlayer


    override val roomView: RoomView
        get() {
            return if (contributorMode) {
                StoryViewContrib(this, roomId)
            }
            else {
                StoryView(this, roomId)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        mStoryBinding = AwesomeActivityStoryDetailBinding.inflate(layoutInflater)

        super.onCreate(savedInstanceState)
    }

    private val contributorMode: Boolean
        get() = intent.getBooleanExtra(EXTRA_CONTRIBUTOR_MODE, false)

    override fun applyStyleForToolbar() {
        super.applyStyleForToolbar()

        if (!contributorMode) {
            supportActionBar?.title = ""
            toolbar.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    fun sendMedia(mediaUri: Uri?, mimeType: String?) {
        if (mediaUri == null) return

        sendAttachment(mediaUri, mimeType)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_conversation_detail_live, menu)
        return true
    }

    override fun onDestroy() {
        super.onDestroy()

        (roomView as? StoryView)?.pause()
    }
}