package info.guardianproject.keanu.core.ui.gallery

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.widgets.MediaViewHolder
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import timber.log.Timber
import java.io.File
import java.io.FileInputStream

import java.net.URLConnection
import java.util.concurrent.TimeUnit




/**
 * Created by n8fr8 on 2/12/16.
 */
class GalleryMediaViewHolder(view: View, private val context: Context) : MediaViewHolder(view) {

    interface GalleryMediaViewHolderListener {
        fun onImageClicked(viewHolder: GalleryMediaViewHolder, image: Uri)
    }

    private var mListener: GalleryMediaViewHolderListener? = null

    fun setListener(listener: GalleryMediaViewHolderListener?) {
        this.mListener = listener
    }

    fun bind(mimeType: String?, file: String?, sender: String?, timeLine: String?, duration: Long?) {
        if (mimeType != null) {
            container?.visibility = View.VISIBLE

            val mediaUri =
                Uri.parse(file?.split(" ".toRegex())?.toTypedArray()?.firstOrNull() ?: "")

            showMediaThumbnail(mimeType, mediaUri, sender, timeLine, duration)
        }
        else {
            container?.visibility = View.GONE
        }
    }

    private fun showMediaThumbnail(mimeType: String, mediaUri: Uri, sender: String?, timeLine: String?, duration : Long?) {
        @Suppress("NAME_SHADOWING")
        var mimeType = mimeType

        /* Guess the MIME type in case we received a file that we can display or play. */
        if (mimeType.isEmpty() || mimeType.startsWith("application")) {
            val guessed = URLConnection.guessContentTypeFromName(mediaUri.toString())

            if (guessed.isNotEmpty()) {
                mimeType = if (TextUtils.equals(guessed, "video/3gpp")) "audio/3gpp" else guessed
            }
        }

        mediaThumbnail?.setOnClickListener { mListener?.onImageClicked(this, mediaUri) }
        gallerySender?.text = sender
        galleryTime?.text = timeLine

        mediaThumbnail?.visibility = View.VISIBLE

        when {
            mimeType.startsWith("image/") -> {
                setImageThumbnail(mediaUri)
                videoIcon?.visibility = View.GONE
                videoDuration?.visibility = View.GONE
            }

            mimeType.startsWith("video/") -> {
                setImageThumbnail(mediaUri)
                videoIcon?.visibility = View.VISIBLE
                videoDuration?.visibility = View.VISIBLE
                videoDuration?.text = String.format(
                    "%02d:%02d", duration?.let { TimeUnit.MILLISECONDS.toMinutes(it) },duration?.let {
                        TimeUnit.MILLISECONDS.toSeconds(it) % 60 })
            }

            mimeType.startsWith("audio") -> {
                videoIcon?.visibility = View.GONE
                videoDuration?.visibility = View.GONE
                mediaThumbnail?.setImageResource(R.drawable.media_audio_play)
                mediaThumbnail?.setBackgroundColor(Color.TRANSPARENT)
            }

            else -> {
                mediaThumbnail?.setImageResource(R.drawable.file_unknown) // generic file icon
            }
        }
    }

    /**
     * @param mediaUri
     */
    @SuppressLint("CheckResult")
    private fun setImageThumbnail(mediaUri: Uri) {
        val view = mediaThumbnail ?: return

        // Pair this holder to the uri. If the holder is recycled, the pairing is broken.
        this.attachment = mediaUri

        view.setImageResource(R.drawable.icon_download)

        val glideRb = Glide.with(context).asDrawable()

        when {
            SecureMediaStore.isVfsUri(mediaUri) -> {
                try {
                    val path = mediaUri.path
                    val fileMedia = if (path != null) File(path) else null

                    if (fileMedia?.exists() == true) {
                        Timber.d("VFS loading: $mediaUri")

                        glideRb.load(FileInputStream(fileMedia))
                    } else {
                        Timber.d("VFS file not found: $mediaUri")

                        glideRb.load(R.drawable.broken_image_large)
                    }
                } catch (e: Exception) {
                    Timber.e(e, "Unable to load thumbnail.")

                    glideRb.load(R.drawable.broken_image_large)
                }
            }

            mediaUri.scheme == "asset" -> {
                val assetPath = "file:///android_asset/" + mediaUri.path?.substring(1)

                glideRb.load(assetPath)
            }

            mediaUri.scheme == "mxc" -> {
                val url = ImApp.sImApp?.matrixSession?.contentUrlResolver()?.resolveThumbnail(
                    mediaUri.toString(),
                    256.coerceAtLeast(view.measuredWidth),
                    256.coerceAtLeast(view.measuredHeight),
                    ContentUrlResolver.ThumbnailMethod.CROP
                )

                if (url != null) {
                    glideRb.load(Uri.parse(url))
                } else {
                    glideRb.load(R.drawable.broken_image_large)
                }
            }

            else -> {
                glideRb.load(mediaUri)
            }
        }

        glideRb.apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
            .into(view)
    }
}